Setup a fresh installed Fedora box for development.

Mainly Elixir programming using neovim, tmux, and VS Codium.

```sh
make setup
make run
```

Happy hacking!
